<?php // $this->layout=false; ?>
<script>
    window.print();
</script>
<?php //echo $this -> Html -> link($this -> Html -> tag('i', '', array('class' => 'fa fa-cart-plus')) . "&nbsp;<span>Cetak</span>", array('action' => ''), array('escape' => false, 'class' => 'btn btn-xs btn-success','id'=>'btnPrint')); 
		echo $this -> Html -> script(array('lib/min/jquery-min'));

?> 
<script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js" > </script>
<!--<button id="btnPrint" class="btn btn-xs btn-success"><i class="fa fa-print"></i>&nbsp;Cetak</button>-->
<div id="print">
<table style="font-size: 13px">
	<tr>
		<td>No.Nota</td><td>: <?php echo $cek[0]['penjualans']['nomor']?></td>
		<td width="20%">&nbsp;&nbsp;&nbsp;</td>
		<td>Nama</td><td>: <?php echo $cek[0]['customers']['nmplg']?></td>
	</tr>
	<tr>
		<td>No.Order</td><td>: <?php echo $cek[0]['penjualans']['noorder']?></td>
		<td width="20%">&nbsp;&nbsp;&nbsp;</td>
		<td>Alamat</td><td>: <?php echo $cek[0]['customers']['alamat']?></td>
	</tr>
	<tr>
		<td>Tanggal</td><td>: <?php echo date("d/m/Y",strtotime($cek[0]['penjualans']['created']))?></td>
		<td width="20%">&nbsp;&nbsp;&nbsp;</td>
		<td>Telp/Hp</td><td>: <?php echo $cek[0]['customers']['telp']."/".$cek[0]['customers']['hp']?></td>
	</tr>
	<tr>
		<!-- <td>Jatuh Tempo</td><td>: <?php echo date("d/m/Y",strtotime($cek[0]['bayars']['jatuh_tempo']))?></td> -->
		<td>No. Mesin</td><td>: <?php echo $cek[0]['penjualans']['nomesin']?></td>
		<td width="20%">&nbsp;&nbsp;&nbsp;</td>
		<td>Tipe Bayar</td><td>: <?php echo $cek[0]['bayars']['tipe_bayar']?></td>
	</tr>
	<tr>
		<td>No Polisi</td><td>: <?php echo $cek[0]['penjualans']['nopol']?></td>
		<td width="20%">&nbsp;&nbsp;&nbsp;</td>
		<td>Merk/Tipe Mobil</td><td>: <?php echo $cek[0]['merks']['merk']."/".$cek[0]['model']['model']?></td>
	</tr>
	<tr>
		<td>No. Rangka</td><td>: <?php echo $cek[0]['penjualans']['norangka']?></td>
		<td width="20%">&nbsp;&nbsp;&nbsp;</td>
		<td></td><td></td>
	</tr>
</table>
    <table class="table table-bordered" style="font-size:12px;width:650px;" id="markb">
	<tr  style="background-color: #ccc;border:1px solid gray">
		<td id="mark" >Bagian</td>
		<td id="mark" >Nama Barang</td>
		<td id="mark" width="5%" >Qty</td>
		<td id="mark" >Harga</td>
		<td id="mark" >Disc</td>
		<td id="mark" >Subtotal</td>
	</tr>
<?php $total="";foreach ($cek as $d){
    if($d['detail_penjualans']['tipedisc']==1){
    $discitem=($d['detail_penjualans']['qty']*$d['detail_penjualans']['harga'])*($d['detail_penjualans']['disc_item']/100);
    $sdisc=$d['detail_penjualans']['disc_item']." %";
    }else{
    $discitem=$d['detail_penjualans']['disc_item'];
    $sdisc=number_format($d['detail_penjualans']['disc_item'],0,',','.');
    }
    $subtotal=$d['detail_penjualans']['qty']*$d['detail_penjualans']['harga'];
    
    ?>
        <tr> 
		<td id="mark"  ><?php echo $d['categories']['kategori']?></td>
		<td id="mark"><?php echo $d['products']['nama_produk']?></td>
		<td id="mark"><?php echo $d['detail_penjualans']['qty']?></td>
		<td id="mark" align="right" ><?php echo number_format($d['detail_penjualans']['harga'],0,',','.')?></td>
		<td id="mark"  align="right" ><?php echo $sdisc;?></td>
		<td id="mark"  align="right" ><?php echo number_format($subtotal-$discitem,0,',','.')?></td>
	</tr>
<?php $total += $subtotal-$discitem;
		}
//                print_r($cek);
            	if($cek[0]['penjualans']['tipedisc']==1){
					$discall=$cek[0]['penjualans']['disc']." %";
					// $disall=$cek[0]['penjualans']['disc'];
					$tdiscall=($total*($cek[0]['penjualans']['disc']/100));
				}else{
					$discall=number_format($cek[0]['penjualans']['disc'],0,',','.');
					$tdiscall=$cek[0]['penjualans']['disc'];
					
				}
            	if($cek[0]['penjualans']['tipehiddisc']==1){
					$dischid=$cek[0]['penjualans']['hidden_disc']." %";
					$tdischid=($total*($cek[0]['penjualans']['hidden_disc']/100));
				}else{
					$dischid=number_format($cek[0]['penjualans']['hidden_disc'],0,',','.');
					$tdischid=$cek[0]['penjualans']['hidden_disc'];
					
				}	
		$totdiskon=$total-($tdiscall+$tdischid);
				?>	

	<tr>
		<td>Total</td><td align="right"><?php echo number_format($total,0,',','.')?></td>
		<td colspan="3">Diskon</td>
		<td align="right"><?php echo number_format($tdiscall+$tdischid,0,',','.');?></td>

	</tr>

</table>
    
<table class="table table-bordered" style="font-size:11px;width: 650px;" id="markb">
	<tr >
		<td id="mark" style="background-color: #ccc" >Grand Total</td>
		<td id="mark" align="right" ><?php echo number_format($total-($tdiscall+$tdischid),0,',','.')?></td>
		<td id="mark" style="background-color: #ccc" >Sudah Bayar Total</td>
		<td id="mark" align="right" ><?php echo number_format($sudahbayar,0,',','.');?></td>
	</tr>
	<tr >
		<td id="mark" style="background-color: #ccc" >Bayar</td>
		<td id="mark" align="right" ><?php echo number_format($d['bayars']['bayar'],0,',','.')?></td>
                <td  id="mark" style="background-color: #ccc" ><?php
			if ($sudahbayar < ($total - ($tdiscall+$tdischid))) { echo "Kurang Bayar";
			} else {echo "Uang Kembali";
			}
		?></td>
                <td id="mark" align="right" ><?php echo str_replace("-", "", number_format(($total-($tdiscall+$tdischid))-$sudahbayar,0,',','.'));?></td>
	</tr>
</table>
</div>
<?php
		echo $this -> Html -> script(array('lib/jquery.printElement.min'));
	?>
<script type="text/javascript">
       $(document).ready(function() {
         $("#btnPrint").click(function() {
             printElem({});
         });

     });
 function printElem(options){
     $('#print').printElement(options);
 }

    </script>
  <style>
@media print 
{
  @page { margin: 0; }
  body  { margin: 1.6cm;font-size:12px; }
  #mark{
      border-top: 1px solid #ddd;
      /*border-bottom: 1px solid #ddd;*/
      border-left: 1px solid #ddd;
      border-right: 1px solid #ddd;
      padding: 10px;
  }
   #markb{
          padding-top: 10px;
           border-bottom: 1px solid #ddd;
  }
  
 
  
}
 </style>   