<?php $this -> layout = false;
// debug($array);
if ($tipe == 'd') {
	$tipedis = $array['disdepan'];
} elseif ($tipe == 'sa') {
	$tipedis = $array['dissamping'];
} elseif ($tipe == 'b') {
	$tipedis = $array['disbelakang'];
} elseif ($tipe == 'a') {
	$tipedis = $array['disaksesoris'];
} elseif ($tipe == 'se') {
	$tipedis = $array['disservice'];
}
?>
<?php echo $this -> Form -> create('Penjualans', array('id' => 'j-forms-validation', 'class' => 'form-horizontal j-forms')); ?>
<?php echo $array['nama']; ?>
<div class="form-group">
	<table class="table">
		<tr><th>Qty</th><th>Harga</th><th>Diskon</th></tr>
	</table>
                                    <!-- <div class=" col-md-4 unit">
                                        <?php echo $this -> Form -> input('id_product', array('class' => 'form-control', 'label' => false)); ?>
                                    </div> -->
                                    <input type="hidden" value="<?php echo $array['id'];?>|ed" id="edidses">
                                    <input type="hidden" value="<?php echo $tipe;?>" id="katitem">
                                    <div class="col-md-3 unit " style="margin-right: -20px">
                                        <?php echo $this -> Form -> input('qty', array('id'=>'edqty','style' => 'width:90%;', 'div' => false, 'class' => 'form-control', 'label' => false, 'value' => $array['jml'], 'type' => 'number', 'min' => '0')); ?>
                                    </div>
                                    <div class="col-md-5 unit"  style="margin-right: -45px">
                                        <?php echo $this -> Form -> input('harga', array('id'=>'edharga','style' => 'width:80%;margin-right:-10px;', 'div' => false, 'class' => 'col-xs-2 form-control', 'label' => false, 'value' => $array['harga'], 'type' => 'number', 'min' => '0')); ?>
                                    </div>
							        <div class="col-md-4 input select " style="margin-right: -89px">
							            <?php echo $this -> Form -> input('disdepan', array('options' => array("1" => "%", "2" => "Rp"), 'min' => 0, 'style' => 'width:45	%;margin-right:-10px;', 'div' => false, 'id' => 'edtipedis', 'selected' => $tipedis, 'class' => 'form-control', 'label' => false)); ?>
							        </div>         
							        <div class="col-md-4 unit" >
                                        <?php echo $this -> Form -> input('disc', array('id'=>'eddisc','style' => 'width:100%;margin-right:-10px;', 'div' => false, 'class' => 'col-xs-2 form-control', 'label' => false, 'value' => $array['diskon'], 'type' => 'number', 'min' => '0')); ?>
 
 
                                </div>
<?php if ($tipe == 'd') { ?>
	      <div class="modal-footer">
                                    	<div class=" col-md-12 unit">
								<button class="btn btn-success" data-dismiss="modal" aria-hidden="true" id="simpanedit">Simpan</button>
							
								</div>     

      </div>
<?php } elseif ($tipe == 'sa') {?>
	      <div class="modal-footer">
                                    	<div class=" col-md-12 unit">
								<button class="btn btn-success" data-dismiss="modal" aria-hidden="true" id="simpaneditsam">Simpan</button>
							
								</div>     

      </div>
<?php } elseif ($tipe == 'b') {?>
	      <div class="modal-footer">
                                    	<div class=" col-md-12 unit">
								<button class="btn btn-success" data-dismiss="modal" aria-hidden="true" id="simpaneditbel">Simpan</button>
							
								</div>     

      </div>
<?php } elseif ($tipe == 'a') {?>
	      <div class="modal-footer">
                                    	<div class=" col-md-12 unit">
								<button class="btn btn-success" data-dismiss="modal" aria-hidden="true" id="simpaneditaks">Simpan</button>
							
								</div>     

      </div>
<?php } elseif ($tipe == 'se') {?>
	      <div class="modal-footer">
                                    	<div class=" col-md-12 unit">
								<button class="btn btn-success" data-dismiss="modal" aria-hidden="true" id="simpaneditser">Simpan</button>
							
								</div>     

      </div>
<?php }?>
<script type="text/javascript">
  
      $('#simpanedit').on('click', function () {
      		var norandom = $("#norandom").val();
	        var id = $('#edidses').val();
	        var tipe = $('#katitem').val();
	        var qty = $('#edqty').val();
	        var harga = $('#edharga').val();
	        var tipedis = $('#edtipedis').val();
	        var diskon = $('#eddisc').val();
	            $.ajax({
	                type: "POST",
	                url: "<?php echo $this -> webroot; ?>penjualans/cart/"+norandom,
       				data: {norandom:norandom ,idp : id,jml :qty,harga :harga,disdepan :tipedis,diskon :diskon },
		            success: function (html) {
	                   $("#isi_cart").html(html);
        			$("#PenjualanTotal").load('<?php echo $this->webroot; ?>Penjualans/jumlahtot/'+norandom);
	            }
	        });
        });
      $('#simpaneditsam').on('click', function () {
      		var norandom = $("#norandom").val();
	        var id = $('#edidses').val();
	        var tipe = $('#katitem').val();
	        var qty = $('#edqty').val();
	        var harga = $('#edharga').val();
	        var tipedis = $('#edtipedis').val();
	        var diskon = $('#eddisc').val();
	            $.ajax({
	                type: "POST",
	                url: "<?php echo $this -> webroot; ?>penjualans/samping/"+norandom,
       				data: {norandom:norandom ,idp : id,jml :qty,harga :harga,dissamping :tipedis,diskon :diskon },
		            success: function (html) {
       			   $("#samping_view").html(html);
      				  $("#PenjualanTotal").load('<?php echo $this->webroot; ?>Penjualans/jumlahtot/'+norandom);

	            }
	        });
        });
      $('#simpaneditbel').on('click', function () {
      		var norandom = $("#norandom").val();
	        var id = $('#edidses').val();
	        var tipe = $('#katitem').val();
	        var qty = $('#edqty').val();
	        var harga = $('#edharga').val();
	        var tipedis = $('#edtipedis').val();
	        var diskon = $('#eddisc').val();
	            $.ajax({
	                type: "POST",
	                url: "<?php echo $this -> webroot; ?>penjualans/belakang/"+norandom,
       				data: {norandom:norandom ,idp : id,jml :qty,harga :harga,disbelakang :tipedis,diskon :diskon },
		            success: function (html) {
       			   	$("#belakang_view").html(html);
       				 $("#PenjualanTotal").load('<?php echo $this->webroot; ?>Penjualans/jumlahtot/'+norandom);

	            }
	        });
        });
      $('#simpaneditaks').on('click', function () {
      		var norandom = $("#norandom").val();
	        var id = $('#edidses').val();
	        var tipe = $('#katitem').val();
	        var qty = $('#edqty').val();
	        var harga = $('#edharga').val();
	        var tipedis = $('#edtipedis').val();
	        var diskon = $('#eddisc').val();
	            $.ajax({
	                type: "POST",
	                url: "<?php echo $this -> webroot; ?>penjualans/aksesoris/"+norandom,
       				data: {norandom:norandom ,idp : id,jml :qty,harga :harga,disaksesoris :tipedis,diskon :diskon },
		            success: function (html) {
       			   	$("#aksesoris_view").html(html);
       				 $("#PenjualanTotal").load('<?php echo $this->webroot; ?>Penjualans/jumlahtot/'+norandom);
	            }
	        });
        });
      $('#simpaneditser').on('click', function () {
      		var norandom = $("#norandom").val();
	        var id = $('#edidses').val();
	        var tipe = $('#katitem').val();
	        var qty = $('#edqty').val();
	        var harga = $('#edharga').val();
	        var tipedis = $('#edtipedis').val();
	        var diskon = $('#eddisc').val();
	            $.ajax({
	                type: "POST",
	                url: "<?php echo $this -> webroot; ?>penjualans/service/"+norandom,
       				data: {norandom:norandom ,idp : id,jml :qty,harga :harga,disservice :tipedis,diskon :diskon },
		            success: function (html) {
       			   	$("#service_view").html(html);
       				 $("#PenjualanTotal").load('<?php echo $this->webroot; ?>Penjualans/jumlahtot/'+norandom);
	            }
	        });
        });
</script>