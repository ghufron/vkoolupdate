<ul class="list-accordion">
    <li>
    	<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'zmdi zmdi-home zmdi-hc-fw')) . "<span class='list-label'>Home</span>", array('controller' => 'pages', 'action' => 'home'), array('escape' => false,'class'=>(($this->params['controller']=='pages')&& ($this->params['action']=='display') )?'active' :'' )); ?>  
    </li>
    <li>
        <a href="#"><i class="zmdi zmdi-shopping-cart zmdi-hc-fw"></i><span class="list-label">Transaksi</span></a>
        <ul>
            <li> <?php echo $this->Html->link("Add Penjualan", array('controller' => 'Penjualans', 'action' => 'add'), array('escape' => false,'class'=>(($this->params['controller']=='Penjualans')&& ($this->params['action']=='add') )?'active' :'')); ?></li>
            <li> <?php echo $this->Html->link("Penjualan", array('controller' => 'Penjualans', 'action' => 'index'), array('escape' => false,'class'=>(($this->params['controller']=='Penjualans')&& ($this->params['action']=='index') )?'active' :'')); ?></li>
        </ul>
    </li>
</ul>