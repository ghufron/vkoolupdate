<?php $this -> layout = false;
// debug($_SESSION["cart_item"])
?>
<style>
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
		padding:0 2px 0 2px;
	}
</style>
<table class="table">
	<thead>
		<tr>
			<th>ID Produk</th>
			<th>Nama</th>
			<th>Qty</th>
			<th>Dimensi</th>
			<th>Harga Satuan</th>
			<th>Disc</th>
			<th>Subtotal</th>
			<th>Hapus</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if (!empty($_SESSION["cart_item"])) {
			$x = 0;
			$total = "";
			// debug($_SESSION["cart_item"]);
			foreach ($_SESSION["cart_item"] as $k => $item) {

				// foreach ($_SESSION["cart_item"] as $item){
				if ($item['id'] != NULL) {
					if ($item['discitem'] == 1) {
						$disc = $item['potitem'] / 100;
						$tdisc=$item['potitem']." %";
						$pot = (str_replace(".", "", $item['harga']) * $item['jml']) * $disc;
					} else {
						$tdisc=number_format($item['potitem'], 0, ',', '.');
						$pot = $item['potitem'];
					}

					if ($item['dimensi'] != NULL) {
						$dimensi = explode(",", $item['dimensi']);
						$d = $dimensi[0] . " x " . $dimensi[1];
					} else {
						$d = "";
					}
					echo "<tr><td>" . $item['id'] . "</td><td>" . $item['nama'] . "</td>
	<td>" . $item['jml'] . "</td><td>" . $d . "</td><td align='right'>" . number_format($item['harga'], 0, ',', '.') . "</td><td align='right'>".$tdisc."</td><td align='right'>" . number_format(((str_replace(".", "", $item['harga']) * $item['jml']) - $pot), 0, ',', '.') . "</td>
	<td><button type='button' class='btn btn-xs btn-danger' id='" . $k . "' onClick='configurator(this)'><i class='fa fa-trash-o'></i></button>
	<a class='btn btn-xs btn-info' id='" . $k . "' onClick='editses(this)'  href='#myModal' role='button' class='btn btn-xs btn-info' data-toggle='modal'><i class='fa fa-pencil'></i></a>
	</td></tr>";
				}
				$x++;
				$total += (str_replace(".", "", $item['harga']) * $item['jml']) - $pot;
			}
		}
	?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="6">
			<div align="right">
				Total
			</div></td>
			<td>
			<div class=" col-xs-6" align='right'>
				<?php
				if (!empty($total)) {echo number_format($total, 0, ',', '.');
				}
				?>
			</div></td><td></td>
		</tr>

		<tr>
			<td colspan="6">
			<div align="right">
				Discount
			</div></td>
			<td colspan="2">
			<div class=" col-xs-5" align='right'>
				<?php echo $this -> Form -> input('potongan', array('placeholder' => '0', 'onFocus' => 'startCalc();', 'onBlur' => 'stopCalc();', 'id' => 'potongan', 'class' => 'form-control', 'label' => false)); ?>
			</div>
			<div class=" col-xs-6" align='right' style="margin-left: -93px">
            	<?php echo $this->Form->input('diskonall', array('options'=>array("1"=>"%","2"=>"Rp"),'min'=>0,'style'=>'width:60%;margin-right:-10px;','div'=>false,'id'=>'discall','onChange'=>'startCalc()','class' => 'form-control', 'label' => false)); ?>
			</div>
			</td>
		</tr>
		<tr>
			<td colspan="6">
			<div align="right">
				Total setelah discount
			</div></td>
			<td>
			<div class=" col-xs-6" align='right'>
				<?php echo $this -> Form -> input('total', array('readonly' => true, 'id' => 'total', 'class' => 'form-control', 'label' => false)); ?>
			</td><td></td>
		</tr>
		<tr>
			<td colspan="6">
			<div align="right">
				Biaya Kirim
			</div></td>
			<td>
			<div class=" col-xs-6" align='right'>
				<?php echo $this -> Form -> input('kirim', array('placeholder' => '0', 'onFocus' => 'startCalc();', 'onBlur' => 'stopCalc();', 'id' => 'kirim', 'class' => 'form-control money-mask', 'label' => false)); ?>
			</td><td></td>
		</tr>
		<tr>
			<td colspan="6">
			<div align="right">
				PPN
			</div></td>
			<td>
			<div class=" col-xs-6" align='right'>
				<?php echo $this -> Form -> input('ppn', array('onFocus' => 'startCalc();', 'onBlur' => 'stopCalc();', 'id' => 'ppn', 'class' => 'form-control money-mask', 'label' => false, 'value' => 0)); ?>
			</td><td></td>
		</tr>
		<tr>
			<td colspan="6">
			<div align="right">
				Grandtotal
			</div></td>
			<td>
			<div class=" col-xs-6" align='right'>
				<?php echo $this -> Form -> input('grandtotal', array('placeholder' => '0', 'readonly' => true, 'id' => 'grandtotal', 'class' => 'form-control money-mask', 'label' => false)); ?>
			</td><td></td>
		</tr>
	</tfoot>
</table>
<script type="text/javascript">
	function configurator(clicked) {
// alert(id)
var id = clicked.id;
$.ajax({
type: "POST",
url: "<?php echo $this -> webroot; ?>Pembelians/del_produk/",
data: { idp : id },
success: function(html) {
jq("#isi_cart").html(html);
}
});
}

</script>
<script>
	function startCalc() {
interval = setInterval("calc()", 1);
}

function numberWithCommas(x) {
var parts = x.toString().split(".");
parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
return parts.join(".");
}

function calc() {
var disc = document.getElementById('discall').value;
// alert(disc)	
var a = document.getElementById('potongan').value;
if(disc==1){
	var d = (<?php echo $total; ?>)*(a/100);
}else{
	var d = a.replace(/,/g, '');
}
var b = document.getElementById('kirim').value;
var e = b.replace(/,/g, '');
var c = document.getElementById('ppn').value;
var f = c.replace(/,/g, '');
var k = (<?php echo $total; ?>)-(d*1);
var gt = (<?php echo $total; ?>-d)+(e * 1) + (f * 1);
	document.getElementById('total').value = numberWithCommas(Math.round(k));
	document.getElementById('grandtotal').value = numberWithCommas(Math.round(gt));
	}

	function stopCalc() {
		clearInterval(interval);
	}
</script>