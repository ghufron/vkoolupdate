<?php $this -> layout = false;
//debug($data);
?>
<?php echo $this -> Form -> create('Bahanbakus', array('id' => 'j-forms-validation', 'class' => 'form-horizontal j-forms')); ?>
<h4><?php echo $data['Product']['nama_produk'];?></h4>
<div class="form-group">

	<div class=" col-md-4 unit">
		<input type="hidden" id="idpro" value="<?php echo $data['Product']['id']?>" />
		<?php echo $this -> Form -> input('pjg', array('id' => 'pjgsisa', 'class' => 'form-control', 'label' => false,'placeholder'=>'panjang')); ?>
	</div>

	<div class=" col-md-4 unit">
		<?php echo $this -> Form -> input('lbr', array('id' => 'lbrsisa', 'class' => 'form-control', 'label' => false,'placeholder'=>'lebar')); ?>

	</div>
	<div class=" col-md-4 unit">
	<button class="btn btn-success" data-dismiss="modal" aria-hidden="true" id="simpansisa">Simpan</button>

	</div>
</div>
   <script type="text/javascript">
  
      $('#simpansisa').on('click', function () {
	        var p = $('#pjgsisa').val();
	        var l = $('#lbrsisa').val();
	        var id = $('#idpro').val();
	            $.ajax({
	                type: "POST",
	                url: "<?php echo $this -> webroot; ?>bahanbakuses/addsisa/",
	            data: {1:id,2: p,3:l },
	            success: function (html) {
	                jq("#isisisa").html(html);
	            }
	        });
        });
</script>